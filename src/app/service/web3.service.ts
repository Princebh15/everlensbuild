import { Injectable } from '@angular/core';

const Everlens = require('../../../build/contracts/Everlens.json');
const AuctionRepository = require('../../../build/contracts/AuctionRepository.json');
import {Subject} from 'rxjs'
declare var require;
const Web3 = require('web3');
declare let window: any;

@Injectable({
  providedIn: 'root'
})
export class Web3Service {

  // isAllData = new Subject<any>();
  searchData= new Subject<any>();
  clearSearch= new Subject<any>();

  private messageResult: any;
  utils: any;

  constructor() {
  }

  public checkAndInstantiateWeb3(): Promise<string> {
    return new Promise((resolve, reject) => {
      if (window.ethereum) {
        this.messageResult = 'connected';
        window.web3 = new Web3(window.ethereum);
        window.ethereum.enable();
        resolve(this.messageResult);
      } else if (window.web3) {
        this.messageResult = 'connected';
        window.web3 = new Web3(window.web3.currentProvider);
        resolve(this.messageResult);
      } else {
        this.messageResult = 'No Binance Smart Chain browser detected. You should consider trying MetaMask';
        reject(this.messageResult);
      }
    });
  }

  public loadBlockChainData(): Promise<string> {
    return new Promise((resolve, reject) => {
      const web3 = window.web3;
      const account = web3.eth.getAccounts();
      if (account !== undefined) {
        resolve(account);
      } else {
        this.messageResult = 'There is no account';
        reject(this.messageResult);
      }
    });
  }

  public getContract() {    
    return new Promise((resolve) => {
      const web3 = window.web3;
      let networkId;
      if(web3) {
        web3.eth.net.getId()
          .then((netId: any) => {
            networkId = netId;
            const abi = Everlens.abi;
            const networkAddress = Everlens.networks[networkId].address;
            const marketplace = new web3.eth.Contract(abi, networkAddress);
            resolve(marketplace);
          });
      }
    });
  }

  public getRepositoryAddress() {    
    return new Promise((resolve) => {
      const web3 = window.web3;
      let networkId;
      if(web3){
      web3.eth.net.getId()
        .then((netId: any) => {
          networkId = netId;
          const abi = Everlens.abi;
          const networkAddress = Everlens.networks[networkId].address;
          resolve(networkAddress);
        });
      }
    });
  }

  public getNetworkId(){
    return new Promise((resolve) => {
      const web3 = window.web3;
      let networkId;
      if(web3){
      web3.eth.net.getId()
        .then((netId: any) => {
          networkId = netId;
          resolve(networkId);
        });
      }
    });
  }

  public getValueToWei(ether) {    
    return new Promise((resolve) => {
      const web3 = window.web3;
      resolve(web3.utils.toWei(ether, 'ether'));
    });
  }

  public getValueFromWei(wei){
    return new Promise((resolve) => {
      const web3 = window.web3;
      resolve(web3.utils.fromWei(wei, 'ether'));
    });
  }


  public getContractAddress() {    
    return new Promise((resolve) => {
      const web3 = window.web3;
      let networkId;
      if(web3){
      web3.eth.net.getId()
        .then((netId: any) => {
          networkId = netId;
          const abi = Everlens.abi;
          const networkAddress = Everlens.networks[networkId].address;
          resolve(networkAddress);
        });
      }
    });
  }


  public getAuctionContract() {  
    return new Promise((resolve) => {
      const web3 = window.web3;
      let networkId;
      if(web3){
      web3.eth.net.getId()
        .then((netId: any) => {
          networkId = netId;
          const abi = AuctionRepository.abi;
          const networkAddress = AuctionRepository.networks[networkId].address;
          const auction = new web3.eth.Contract(abi, networkAddress);
          resolve(auction);
        });
      }
    });
  }



  public convertPriceToEther(price) {
    const web3 = window.web3;
    return web3.utils.toWei(price.toString(), 'Ether');
  }

  // public convertEtherToPrice(price) {
  //   const web3 = window.web3;
  //   return web3.utils.fromWei(price, 'Ether');
  // }

  public getEtherBalance(account) {

    return new Promise((resolve) => {
      const web3 = window.web3;
      if(web3){
      const balance = web3.eth.getBalance(account)
        .then(ba => {
          resolve(web3.utils.fromWei(ba, 'Ether'));
        });
      }
    });

  }

}
