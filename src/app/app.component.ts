import { Component, ChangeDetectorRef, Inject } from '@angular/core';
import { Web3Service } from './service/web3.service';
import { IPFS } from './ipfs/ipfs';
import { HttpClient } from '@angular/common/http';
import { Buffer } from 'buffer';
import * as path from 'path';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { NoMetaMaskComponent } from './@pages/no-meta-mask/no-meta-mask.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  accountNumber: any;
  productName: any;
  productPrice: any;
  show = true;
  totalProduct = [];
  private marketPlace: any;
  balance: any;
  instaImages;
  imageFile: any;
  bsModalRef: BsModalRef;
  showDD: boolean = true;

  constructor(private modalService: BsModalService ,private web3: Web3Service, private cd: ChangeDetectorRef, @Inject(IPFS) private ipfs, private http: HttpClient) {

    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {
        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });
              this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.marketPlace = contractRes;
                    this.marketPlace.methods.productCount()
                      .call()
                      .then(value => {
                        for (let i = 1; i <= value; i++) {
                          const product = this.marketPlace.methods.products(i)
                            .call()
                            .then(products => {
                              this.show = false;
                              this.totalProduct.push(products);
                              this.cd.detectChanges();
                            });
                        }
                      });
                  }
                });
            }, err => {
            });
        }
      }, err => {
        if(err) {
          this.openModal();
        } 
      });
  }

  openModal() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    this.bsModalRef = this.modalService.show(NoMetaMaskComponent,  {
      ignoreBackdropClick: true,
      keyboard: false
    });
    this.hideShowDrop(true);
  }

  hideShowDrop(isShow) {
   this.showDD = isShow;
  }

  private async ensureIpfsUriPrefix(cidOrURI) {
    let uri = cidOrURI.toString()
    uri = 'https://ipfs.io/ipfs/' + cidOrURI
    return uri
  }

  async ngOnInit() {
    const version = await this.ipfs.version();
  }

  /**
     * Helper to construct metadata JSON for 
     * @param {string} assetCid - IPFS URI for the NFT asset
     * @param {object} options
     * @param {?string} name - optional name to set in NFT metadata
     * @param {?string} description - optional description to store in NFT metadata
     * @returns {object} - NFT metadata object
     */
  async makeNFTMetadata(assetURI, options) {
    const { name, description } = options;
    // assetURI = this.ensureIpfsUriPrefix(assetURI)
    return {
      name,
      description,
      image: assetURI
    }
  }

  private async createProducts(name, price) {
    // var res = await fetch(filename);

    this.show = true;
    const etherPrice = this.web3.convertPriceToEther(price);
    const data = {
      name: name,
      price: price,
    }
    const filesAdded = await this.ipfs.add(Buffer.from(JSON.stringify(data)));
    const hash = filesAdded.cid
    const source = await this.ipfs.cat(hash)
    let contents = ''
    const decoder = new TextDecoder('utf-8')

    for await (const chunk of source) {
      contents += decoder.decode(chunk, {
        stream: true
      })
    }

    contents += decoder.decode();
    this.marketPlace.methods.createProduct(name, etherPrice)
      .send({ from: this.accountNumber })
      .once('receipt', (receipt) => {
        this.totalProduct.push(receipt.events.ProductCreated.returnValues);
        this.show = false;
      });
  }

  /**
  * Create a new NFT token that references the given metadata CID, owned by the given address.
  * 
  * @param {string} ownerAddress - the ethereum address that should own the new token
  * @param {string} metadataURI - IPFS URI for the NFT metadata that should be associated with this token
  * @returns {Promise<string>} - the ID of the new token
  */
  private async mintToken(ownerAddress, metadataURI, name, image) {
    let transferId;
    // Call the mintToken method to issue a new token to the given address
    // This returns a transaction object, but the transaction hasn't been confirmed
    // yet, so it doesn't have our token id.
    const tx = await this.marketPlace.methods.createProduct(ownerAddress, metadataURI, name, image).send({ from: this.accountNumber })
      .once('receipt', (receipt) => {
        transferId = receipt.events.ProductCreated.returnValues.id
      });

    return transferId
  }

  private async base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
      bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  private async createNFTFromAssetData(content, options) {
    let that = this;
    const ipfsAddOptions = {
      cidVersion: 1,
      hashAlg: 'sha2-256'
    }
    var d;
    const data = await this.http.get(content, { responseType: 'arraybuffer' })
      .subscribe(async data => {
        var image_data = new Uint8Array(data);
        const filePath = options.path || 'asset.bin'
        const basename = path.basename(filePath)
        const ipfsPath = '/nft/' + basename;
        var assetURI, metadataURI;
        const { cid: assetCid } = await this.ipfs.add({ content: image_data }, ipfsAddOptions)
      
        assetURI = await this.ensureIpfsUriPrefix(assetCid.string)
   
        const metadata = await this.makeNFTMetadata(assetURI, options)
    
        const { cid: assetMCid } = await this.ipfs.add(Buffer.from(JSON.stringify(metadata)), ipfsAddOptions);
        metadataURI = await this.ensureIpfsUriPrefix(assetMCid.string)

        const tokenId = await this.mintToken(this.accountNumber, metadataURI, options.name, content)

        var a = await this.getNFTMetadata(tokenId)

      })

  }

  async getNFT(tokenId, opts) {
    const { metadata, metadataURI } = await this.getNFTMetadata(tokenId)
    const ownerAddress = await this.getTokenOwner(tokenId)
    const metadataGatewayURL = this.makeGatewayURL(metadataURI)
    const nft = { tokenId, metadata, metadataURI, metadataGatewayURL, ownerAddress }

    return nft
  }

  async getCreationInfo(tokenId) {
    const filter = await this.marketPlace.methods.filters.Transfer(
      null,
      null
    )

    const logs = await this.marketPlace.methods.queryFilter(filter)
    const blockNumber = logs[0].blockNumber
    const creatorAddress = logs[0].args.to
    return {
      blockNumber,
      creatorAddress,
    }
  }




  async getTokenOwner(tokenId) {
    return this.marketPlace.methods.ownerOf(tokenId).call({ from: this.accountNumber })
  }

  async getNFTMetadata(tokenId) {
    const metadataURI = await this.marketPlace.methods.tokenURI(tokenId).call({ from: this.accountNumber });
    var uri = metadataURI.slice('https://ipfs.io/ipfs/'.length)
    const source = await this.ipfs.cat(uri)
    let contents = ''
    const decoder = new TextDecoder('utf-8')

    for await (const chunk of source) {
      contents += decoder.decode(chunk, {
        stream: true
      })
    }

    contents += decoder.decode();
    return metadataURI
  }

  async getIPFSJSON(cidOrURI) {
    const str = await this.getIPFSString(cidOrURI)
    return JSON.parse("str")
  }

  async getIPFSString(cidOrURI) {
    const bytes = await this.getIPFS(cidOrURI)
  }

  async stripIpfsUriPrefix(cidOrURI) {
    return cidOrURI
  }

  async getIPFS(cidOrURI) {
    const cid = this.stripIpfsUriPrefix(cidOrURI)
    const fileBuffer = await this.ipfs.cat(cid);
  }

  private async makeGatewayURL(ipfsURI) {
    return `https://ipfs.infura.io:5001/ipfs` + '/' + await this.stripIpfsUriPrefix(ipfsURI)
  }

  private purchaseProducts(id, price) {
    this.show = true;
    this.marketPlace.methods.purchaseProduct(id)
      .send({ from: this.accountNumber, value: price })
      .once('receipt', (receipt) => {
        this.show = false;
      })
      .on('error', (error) => {
        this.show = false;
      });

  }


  trackByFn(index, item) {
    return item.purchased;
  }

  getInstaImages() {

  }

  imagesChange(images) {
    this.imageFile = images
    
  }


}
