import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TermsConditionComponent } from './terms-condition/terms-condition.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { WorkComponent } from './work/work.component';

const routes: Routes = [
  // {
  //   path: '',
  //   loadChildren: () => import('../app/@layouts/blank/blank.module').then((m) => m.BlankModule),
  // },
  
  {
    path : 'terms-conditions',
    component : TermsConditionComponent,
  },
  {
    path : 'privacy-policy',
    component : PrivacyPolicyComponent,
  },
  // {
  //   path : 'works',
  //   component : WorkComponent,
  // },
  {
    path: '',
    loadChildren: () => import('../app/@layouts/basic/basic.module').then((m) => m.BasicModule),
  },
  {
    path: 'everlens',
    loadChildren: () => import('../app/@layouts/basic/basic.module').then((m) => m.BasicModule),
  },
  {
    path: "**",
    loadChildren: () => import("./@pages/not-found/not-found.module").then((m) => m.NotFoundModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
