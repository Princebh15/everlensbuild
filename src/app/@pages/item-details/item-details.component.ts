import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { Web3Service } from 'src/app/service/web3.service';
import { IPFS } from 'src/app/ipfs/ipfs';
import { Buffer } from 'buffer';
@Component({
  selector: 'app-item-details',
  templateUrl: './item-details.component.html',
  styleUrls: ['./item-details.component.scss']
})
export class ItemDetailsComponent implements OnInit {


  show: boolean;
  thumbs: any;
  private auction: any;
  private everlens: any;
  totalProduct = [];
  totalAuction = [];
  accountNumber: any;
  balance: any;
  itemMetadata: any;
  pagination: any = false;
  bidPrice: any;
  // malihu scrollbar options

  public scrollbarOptions = { axis: 'y', theme: 'dark-thin' };

  auction_id: any;
  itemData: any;
  bid: any;
  bidAmount: unknown;
  bidFrom: any;
  startPrice: unknown;
  bidCount: any;
  bids: any;
  product_creator: any;
  product_holder: any;
  endPrice: unknown;
  endDate: string;
  constructor(private activatedRoute: ActivatedRoute, @Inject(IPFS) private ipfs,
    private mScrollbarService: MalihuScrollbarService, private web3: Web3Service,) {

    this.activatedRoute.params.subscribe(param => {
      if (param['id']) {
        this.auction_id = param['id'];
      }
    });

    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {

        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });

                this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.everlens = contractRes;
                  }
                });

              this.web3.getAuctionContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.auction = contractRes;
                    this.getAuctionDetail();
                  }
                });
            })
        }
      }, err => {
        
      });

  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    })
  }

  async placeBid() {
    var ether = await this.web3.getValueToWei(this.bidPrice);

    await this.auction.methods.bidOnAuction(this.auction_id).send({ from: this.accountNumber, value: ether }).then(async (val) => {

    });



  }

  finalizeAuction() {
    this.auction.methods.finalizeAuction(this.auction_id).send({ from: this.accountNumber }).then(async (data) => {
      
    })
  }

  async getAuctionDetail() {
    let auction_data = await this.auction.methods.getAuctionById(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
      this.itemData = data;
      await this.everlens.methods.products(data.productId)
      .call()
      .then(product => {
        this.product_creator = product.creator;
        this.product_holder = product.holder;
       });

       var unixtimestamp = data.endDate;
       var months_arr = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];

       // Convert timestamp to milliseconds
       var date = new Date(unixtimestamp*1000);
      
       // Year
       var year = date.getFullYear();
      
       // Month
       var month = months_arr[date.getMonth()];
      
       // Day
       var day = date.getDate();
      
       // Display date time in MM-dd-yyyy h:m:s format
       var convdataTime = day+' '+month+' '+year;
       this.endDate = convdataTime;

       var ether = await this.web3.getValueFromWei(data.endPrice); 
       this.endPrice = ether;
      this.startPrice = await this.web3.getValueFromWei(data.startPrice);
      var uri = data.metadata.slice('https://ipfs.io/ipfs/'.length)
      const source = await this.ipfs.cat(uri)
      let contents = ''
      const decoder = new TextDecoder('utf-8')

      for await (const chunk of source) {
        contents += decoder.decode(chunk, {
          stream: true
        })
      }

      contents += decoder.decode()

      this.itemMetadata = JSON.parse(contents)

      await this.auction.methods.getCurrentBid(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
    
        var ether = await this.web3.getValueFromWei(data[0]);
        this.bidAmount = ether;
        this.bidFrom = data[1];
      });

      await this.auction.methods.getBidOfAuctions(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
        this.bids = [];
        data.map(async (x) => {
          var ether = await this.web3.getValueFromWei(x[1]);
          this.bids.push({
            amount: ether,
            from: x[0]
          });
        });

      });

      await this.auction.methods.getBidsCount(this.auction_id).call({ from: this.accountNumber }).then(async (data) => {
        this.bidCount = data;
      });
    });
  }

}