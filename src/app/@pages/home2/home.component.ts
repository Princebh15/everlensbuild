import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { SwiperComponent } from "swiper/angular";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

// import Swiper core and required components
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
} from "swiper/core";
import { InstaImagesComponent } from '../modals/insta-images/insta-images.component';
import { Web3Service } from 'src/app/service/web3.service';
import { HttpClient } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';

// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  bsModalRef: BsModalRef;
  instaCode:any;


  @ViewChild("swiperRef", { static: false }) swiperRef?: SwiperComponent;
  show: boolean;
  thumbs: any;
  private everlens: any;
  totalProduct = [];
  accountNumber: any;
  balance: any;
  pagination: any = false;
  
  constructor(private modalService: BsModalService, private cd: ChangeDetectorRef, private web3: Web3Service,
    private http:HttpClient, private router:Router, private activatedRoute:ActivatedRoute) {

      this.activatedRoute.queryParams.subscribe(params => {
        if (params['code']) {
          this.instaCode = params['code'];
          this.getDataWithInstaCode();
          this.ownedProduct();
        }
      });

      


    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {
        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });
                
              this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.everlens = contractRes;
                    this.everlens.methods.productCount()
                      .call()
                      .then(value => {
                        for (let i = 1; i <= value; i++) {
                          const product = this.everlens.methods.products(i)
                            .call()
                            .then(products => {
                              this.show = false;
                              this.totalProduct.push(products);
                              this.cd.detectChanges();
                            });
                        }
                      });
                  }
                });
            }, err => {
            });
        }
      }, err => {
        
      });
  }

  async ownedProduct() {
    
    await this.everlens.methods.balanceOf(this.accountNumber)
      .then((o) => {
        return o.toNumber()
      }).then((balance) => {
        let i = 0;
        let getOwnedNFT = async (i)=>{
         await this.everlens.methods.tokenOfOwnerByIndex(this.accountNumber, i)
        .then((o) => {
          const number = o.toNumber()
          i++;
          if(i<balance){
            getOwnedNFT(i)
          }
        })
      }
    })
  }



  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    })
  }
  breakpoints = {
    320: { slidesPerView: 1, spaceBetween: 20 },
    460: { slidesPerView: 2, spaceBetween: 20 },
    992: { slidesPerView: 3, spaceBetween: 20 },
    1200: { slidesPerView: 4, spaceBetween: 30 }
  };

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    this.bsModalRef = this.modalService.show(InstaImagesComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
  }

  getDataWithInstaCode(){
    const formData = new FormData();
    formData.append('client_id', '2864559007130049');
    formData.append('redirect_uri', 'https://app.everlens.io/everlens/home/');
    formData.append('grant_type', "authorization_code");
    formData.append('client_secret', "6421d5cd8172054c29a597dba73c0702");
    formData.append('code', this.instaCode);
    this.http.post('https://api.instagram.com/oauth/access_token', formData
    ).subscribe((res: any) => {
      // localStorage.setItem("user-session", JSON.stringify(res));
      if (res) {
        localStorage.setItem("access_token", JSON.stringify(res));
        this.router.navigate(['/home']);
      }
      else {
        alert("Something went wrong")
      }
    })
  }


}

function ModalContentComponent(ModalContentComponent: any, arg1: { initialState: { list: string[]; title: string; }; }): BsModalRef<any> {
  throw new Error('Function not implemented.');
}

