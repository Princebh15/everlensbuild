import { Component, OnInit, ViewChild, ChangeDetectorRef, Inject } from '@angular/core';
import { SwiperComponent } from "swiper/angular";
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

// import Swiper core and required components
import SwiperCore, {
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
} from "swiper/core";
import { InstaImagesComponent } from '../modals/insta-images/insta-images.component';
import { Web3Service } from 'src/app/service/web3.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { IPFS } from 'src/app/ipfs/ipfs';

// install Swiper components
SwiperCore.use([
  Navigation,
  Pagination,
  Scrollbar,
  A11y,
  Virtual,
  Zoom,
  Autoplay,
  Thumbs,
  Controller
]);

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  bsModalRef: BsModalRef;
  instaCode: any;

  @ViewChild("swiperRef", { static: false }) swiperRef?: SwiperComponent;
  show: boolean;
  thumbs: any;
  private auction: any;
  private everlens: any;
  totalProduct = [];
  totalAuction = [];
  fameAuction = [];
  hotBidAuction = [];
  accountNumber: any;
  balance: any;
  pagination: any = false;
  startPrice: unknown;
  isLoading: boolean = false;
  seacrhString: string;


  constructor(private modalService: BsModalService, private cd: ChangeDetectorRef, private web3: Web3Service,
    private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute, @Inject(IPFS) private ipfs) {

    // this.activatedRoute.queryParams.subscribe(params => {
    //   if (params['code']) {
    //     this.instaCode = params['code'];
    //     this.getDataWithInstaCode();
    //     // this.ownedProduct();
    //   }
    // });

    this.web3.searchData.subscribe(res => {
      this.seacrhString = res;
    })



    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {
        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });

              this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.everlens = contractRes;
                    this.everlens.methods.productCount()
                      .call()
                      .then(value => {
                        for (let i = 1; i <= value; i++) {
                          const product = this.everlens.methods.products(i)
                            .call()
                            .then(products => {
                              this.show = false;
                              this.totalProduct.push(products);
                              this.cd.detectChanges();
                            });
                        }
                        this.getFameAuctions();
                        this.getHotBids();
                      });
                  }
                });


              this.web3.getAuctionContract()
                .then(async (contractRes: any) => {
                  if (contractRes) {
                    this.isLoading = true;
                    this.auction = contractRes;
                    await this.auction.methods.getCount()
                      .call()
                      .then(async (value) => {
                        for (let i = 0; i < value; i++) {
                          const auction = await this.auction.methods.auctions(i)
                            .call()
                            .then(async (auctions) => {
                              var product_creator, product_holder;
                              await this.everlens.methods.products(auctions.productId)
                                .call()
                                .then(product => {
                                  product_creator = product.creator;
                                  product_holder = product.holder;
                                });

                              var ether = await this.web3.getValueFromWei(auctions.startPrice);
                              await this.everlens.methods.ownerOf(auctions.productId).call({ from: this.accountNumber }).then(async (data) => {

                              });

                              var highestBid = await this.auction.methods.getCurrentBid(i).call({ from: this.accountNumber });
                              var highestEther = await this.web3.getValueFromWei(highestBid[0]);
                              var uri = auctions.metadata.slice('https://ipfs.io/ipfs/'.length)
                              const source = await this.ipfs.cat(uri)
                              let contents = ''
                              const decoder = new TextDecoder('utf-8')

                              for await (const chunk of source) {
                                contents += decoder.decode(chunk, {
                                  stream: true
                                })
                              }

                              contents += decoder.decode()

                              var decoded_data = JSON.parse(contents)
                              // const fileBuffer = await this.ipfs.cat(filesAdded.path);
                              this.show = false;
                              auctions['pro_creator'] = product_creator;
                              auctions['pro_holder'] = product_holder;
                              auctions['meta'] = decoded_data;
                              auctions['minEther'] = ether;
                              auctions['highestBidAmount'] = highestEther;
                              auctions['highestBidFrom'] = highestBid[1];
                              this.show = false;
                              if (auctions.active == true) {
                                this.totalAuction.push(auctions);
                                //   this.isLoading = false;
                              }
                              this.cd.detectChanges();
                            })
                          // });
                        }

                        this.totalAuction = this.totalAuction.sort((a, b) => (+b.id) - (+a.id));
                   

                        this.isLoading = false;
                        //  localStorage.setItem("user-data", JSON.stringify(this.totalAuction));
                        // this.web3.isAllData.next(this.totalAuction);
                      });

                  }
                });
            }, err => {
              this.isLoading = false;
            });
        }
      }, err => {
      });
  }

  async getHotBids() {

    await this.auction.methods.getCount()
      .call()
      .then(async (value) => {
        for (let i = 0; i < value; i++) {
          const auction = await this.auction.methods.auctions(i)
            .call()
            .then(async (auctions) => {
              var ether = await this.web3.getValueFromWei(auctions.startPrice);
              var highestBid = await this.auction.methods.getCurrentBid(i).call({ from: this.accountNumber });
              var highestEther = await this.web3.getValueFromWei(highestBid[0]);
              var product_creator, product_holder;
              await this.everlens.methods.products(auctions.productId)
                .call()
                .then(product => {
                  product_creator = product.creator;
                  product_holder = product.holder;
                });
              var uri = auctions.metadata.slice('https://ipfs.io/ipfs/'.length)
              const source = await this.ipfs.cat(uri)
              let contents = ''
              const decoder = new TextDecoder('utf-8')

              for await (const chunk of source) {
                contents += decoder.decode(chunk, {
                  stream: true
                })
              }

              contents += decoder.decode()

              var decoded_data = JSON.parse(contents)
              if (parseInt(highestBid[0]) > 0 && auctions.active == true) {
                this.show = false;
                auctions['pro_creator'] = product_creator;
                auctions['pro_holder'] = product_holder;
                auctions['meta'] = decoded_data;
                auctions['minEther'] = ether;
                auctions['highestBidAmount'] = highestEther;
                auctions['highestBidFrom'] = highestBid[1];
                this.show = false;
                this.hotBidAuction.push(auctions);
                this.cd.detectChanges();
              }
              // const fileBuffer = await this.ipfs.cat(filesAdded.path);

            })
          // });
        }

        this.hotBidAuction = this.hotBidAuction.sort((a, b) => (+b.highestBidAmount) - (+a.highestBidAmount));
        this.isLoading = false;
      });
  }


  // async ownedProduct() {

  //   await this.everlens.balanceOf(this.accountNumber)
  //     .then((o) => {
  //       return o.toNumber()
  //     }).then((balance) => {
  //       let i = 0;
  //       let getOwnedNFT = async (i) => {
  //         await this.everlens.tokenOfOwnerByIndex(this.accountNumber, i)
  //           .then((o) => {
  //             const number = o.toNumber()
  //             i++;
  //             if (i < balance) {
  //               getOwnedNFT(i)
  //             }
  //           })
  //       }
  //     })
  // }

  async getFameAuctions() {
    await this.auction.methods.getCount()
      .call()
      .then(async (value) => {
        var product_creator, product_holder;
        for (let i = 0; i < value; i++) {
          const auction = await this.auction.methods.auctions(i)
            .call()
            .then(async (auctions) => {
              var bidCount = await this.auction.methods.getBidsCount(i).call({ from: this.accountNumber })
              var ether = await this.web3.getValueFromWei(auctions.endPrice);
              this.show = false;
              await this.everlens.methods.products(auctions.productId)
                .call()
                .then(product => {
                  product_creator = product.creator;
                  product_holder = product.holder;
                });
              auctions['creator'] = product_creator;
              auctions['holder'] = product_holder;
              auctions['highestEther'] = ether;
              auctions['bidCount'] = bidCount;
              if (auctions.active == false) {
                this.fameAuction.push(auctions);
              }
              this.cd.detectChanges();
            })
        }

        this.fameAuction = this.fameAuction.sort((a, b) => (+b.highestEther) - (+a.highestEther));
        this.isLoading = false;
      });
  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    })
  }
  breakpoints: any;

  openModalWithComponent() {
    const initialState = {
      list: [
        'Open a modal with component',
        'Pass your data',
        'Do something else',
        '...'
      ],
      title: 'Modal with component'
    };
    this.bsModalRef = this.modalService.show(InstaImagesComponent, { initialState });
    this.bsModalRef.content.closeBtnName = 'Close';
  }



  getDataWithInstaCode() {
    const formData = new FormData();
    formData.append('client_id', '2864559007130049');
    formData.append('redirect_uri', 'https://app.everlens.io/home/');
    formData.append('grant_type', "authorization_code");
    formData.append('client_secret', "6421d5cd8172054c29a597dba73c0702");
    formData.append('code', this.instaCode);
    this.http.post('https://api.instagram.com/oauth/access_token', formData
    ).subscribe(async (res: any) => {
      // localStorage.setItem("user-session", JSON.stringify(res));
      if (res) {
        await this.http.get('https://graph.instagram.com/access_token', {
          params: {
            "grant_type": "ig_exchange_token",
            "client_secret": "6421d5cd8172054c29a597dba73c0702",
            "access_token": res.access_token
          }
        }).subscribe((res: any) => {
          if (res) {
            localStorage.setItem("access_token", JSON.stringify(res));
          }
          else {
            alert("Something went wrong")
          }
        })

        await this.http.get('https://graph.instagram.com/me/media', {
          params: {
            "fields": "username",
            "access_token": res.access_token
          }
        }).subscribe(async (res: any) => {
          if (res) {

            localStorage.setItem("username", JSON.stringify(res.data[0].username));
            const httpHeader = {
              headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
              })
            }

            // var s =await this.http.get(url, { observe: 'response' }).then((response: Response)=> {
            
            //   return response.json();
            // })
          }
          else {
            alert("Something went wrong")
          }
        })

        this.router.navigate(['/home']);
      }
      else {
        alert("Something went wrong");
      }
    })
  }

  ngOnDestroy() {
    if (this.seacrhString) {
      this.web3.clearSearch.next(true);
    }
  }


}


