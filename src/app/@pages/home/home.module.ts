import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { SwiperModule } from 'swiper/angular';
import { SearchPipe } from '../pipes/search.pipe';

@NgModule({
  declarations: [HomeComponent, SearchPipe],
  imports: [
    CommonModule,
    HomeRoutingModule,
    SwiperModule,
    FormsModule
  ]
})
export class HomeModule { }
