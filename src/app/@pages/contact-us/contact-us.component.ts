import { Component, OnInit, TemplateRef, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { HttpClient } from '@angular/common/http';
import { IPFS } from 'src/app/ipfs/ipfs';
import { Buffer } from 'buffer';
import * as path from 'path';
import { Web3Service } from 'src/app/service/web3.service';
import { ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {

  public scrollbarOptions = { axis: 'y', theme: 'dark-thin' };
  submitting: boolean;
  modalRef: BsModalRef;
  postForm: FormGroup;
  selectedImage: any;
  private everlens: any;
  private auctionn: any;
  instaImages: any[] = [];
  accountNumber: any;
  balance: any;
  show = true;
  totalProduct = [];
  totalAuction = [];
  mintingAuction: boolean = false;
  mintingAuctionCompleted: boolean = false;
  creatingAunction: boolean = false;
  creatingAunctionCompleted: boolean = false;
  access_token1: string;
  // isEditEnable : boolean = true;

  @ViewChild('closeBtn') closeBtn: ElementRef;
  userName: any;
  instaCode: any;
  isLoading: boolean = false;

  constructor(private web3: Web3Service,
    private mScrollbarService: MalihuScrollbarService,
    private http: HttpClient, @Inject(IPFS) private ipfs,
    private cd: ChangeDetectorRef,
    private router: Router, private activatedRoute: ActivatedRoute, private toastrService: ToastrService) {


  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0,
      behavior: 'smooth'
    });

    this.postForm = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.maxLength(70), Validators.email]),
      subject: new FormControl('', [Validators.required, Validators.maxLength(50)]),
      message: new FormControl('', [Validators.required, Validators.maxLength(250)])
    })
  }
  // create Post Method
  contactUs() {
    if (this.postForm.valid) {
      this.http.post("https://app.everlens.io:3000/api/contact-us", this.postForm.value).subscribe(res => {
        let message = "Thanks for contacting us, We will get back to you soon."
        this.toastrService.success(message, '');
        this.postForm.reset();
        // this.router.navigate(['/home']);
      
      }, err => {
      })
    }
  }



}

