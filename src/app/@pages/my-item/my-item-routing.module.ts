import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MyItemComponent } from './my-item.component';

const routes: Routes = [
  {
    path:'',
    component:MyItemComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MyItemRoutingModule { }
