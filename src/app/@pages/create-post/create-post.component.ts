import { Component, OnInit, TemplateRef, Inject, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MalihuScrollbarService } from 'ngx-malihu-scrollbar';
import { HttpClient } from '@angular/common/http';
import { IPFS } from 'src/app/ipfs/ipfs';
import { Buffer } from 'buffer';
import * as path from 'path';
import { Web3Service } from 'src/app/service/web3.service';
import { ViewChild, ElementRef } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";

@Component({
  selector: 'app-create-post',
  templateUrl: './create-post.component.html',
  styleUrls: ['./create-post.component.scss']
})
export class CreatePostComponent implements OnInit {

  public scrollbarOptions = { axis: 'y', theme: 'dark-thin' };
  submitting: boolean;
  modalRef: BsModalRef;
  postForm: FormGroup;
  selectedImage: any;
  private everlens: any;
  private auctionn: any;
  instaImages: any[] = [];
  accountNumber: any;
  balance: any;
  show = true;
  totalProduct = [];
  totalAuction = [];
  mintingAuction: boolean = false;
  mintingAuctionCompleted: boolean = false;
  creatingAunction: boolean = false;
  creatingAunctionCompleted: boolean = false;
  access_token1: string;
  // isEditEnable : boolean = true;

  @ViewChild('closeBtn') closeBtn: ElementRef;
  userName: any;
  instaCode: any;
  isLoading: boolean = false;

  constructor(private web3: Web3Service,
    private mScrollbarService: MalihuScrollbarService,
    private http: HttpClient, @Inject(IPFS) private ipfs,
    private cd: ChangeDetectorRef,
    private router: Router, private activatedRoute: ActivatedRoute) {
    this.getProducts();
    this.activatedRoute.queryParams.subscribe(params => {
      if (params['code']) {
        this.isLoading = true;
        this.instaCode = params['code'];
        this.getDataWithInstaCode();
        // this.ownedProduct();
      }
    });

    // this.getInstaImages();

    this.access_token1 = JSON.parse(localStorage.getItem('access_token'));
    if (this.access_token1) {
      this.getInstaImages();
      this.userName = JSON.parse(localStorage.getItem('username'));
    }

  }

  ngOnInit(): void {

    // window.scrollTo({
    //   top: 0,
    //   left: 0
    // });

    this.postForm = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      description: new FormControl('', [Validators.required, Validators.maxLength(250)]),
      imageFile: new FormControl('', Validators.required),
      minBidAmount: new FormControl('', [Validators.required])
    })
  }
  // create Post Method
  createPost() {
    if (this.postForm.valid) {
      this.createNFTFromAssetData(this.selectedImage, this.postForm.value)
    }
  }


  getDataWithInstaCode() {
    const formData = new FormData();
    formData.append('client_id', '2864559007130049');
    formData.append('redirect_uri', 'https://app.everlens.io/create-post/');
    formData.append('grant_type', "authorization_code");
    formData.append('client_secret', "6421d5cd8172054c29a597dba73c0702");
    formData.append('code', this.instaCode);
    this.http.post('https://api.instagram.com/oauth/access_token', formData
    ).subscribe(async (res: any) => {
      // localStorage.setItem("user-session", JSON.stringify(res));
      if (res) {

        await this.http.get('https://graph.instagram.com/access_token', {
          params: {
            "grant_type": "ig_exchange_token",
            "client_secret": "6421d5cd8172054c29a597dba73c0702",
            "access_token": res.access_token
          }
        }).subscribe(async (res: any) => {
          if (res) {
            localStorage.setItem("access_token", JSON.stringify(res));
            this.access_token1 = JSON.parse(localStorage.getItem('access_token'));
            this.isLoading = false;
            await this.http.get('https://graph.instagram.com/me/media', {
              params: {
                "fields": "id,username",
                "access_token": res.access_token
              }
            }).subscribe(async (res: any) => {

              if (res) {
                localStorage.setItem("username", JSON.stringify(res.data[0].username));
                this.userName = JSON.parse(localStorage.getItem('username'));
              
                this.getInstaImages();
              }
              else {
                alert("Something went wrong")
                this.isLoading = false;
              }
            })
          }
          else {
            alert("Something went wrong")
            this.isLoading = false;
          }
        })



        this.router.navigate(['/create-post']);
      }
      else {
        alert("Something went wrong");
      }
    })
  }


  closeModal(): void {
    this.closeBtn.nativeElement.click();
  }


  getInstaImages() {
    let insta_token = JSON.parse(localStorage.getItem("access_token"));
    let access_token = insta_token.access_token;
    // this.http.get('https://graph.instagram.com/access_token?grant_type=ig_exchange_token&client_secret={instagram-app-secret}&access_token={short-lived-access-token}')

    this.http.get('https://graph.instagram.com/me/media', {
      params: {
        "fields": "id,caption,media_url,media_type,username,children{media_url,thumbnail_url,media_type}",
        "access_token": access_token
      }
    }).subscribe(async (res: any) => {
      if (res) {
        if (res.data.length > 0) {
          for (var i = 0; i < res.data.length; i++) {
            if(res.data[i].media_type=='IMAGE' || res.data[i].media_type=='CAROUSEL_ALBUM'){
            this.instaImages.push(res.data[i].media_url)
            if (res.data[i].children != undefined) {
               if (res.data[i].children.data != undefined) {
                 this.instaImages.pop();
                for (var j = 0; j < res.data[i].children.data.length; j++) {
                  if(res.data[i].children.data[j].media_type=='IMAGE'){
                  this.instaImages.push(res.data[i].children.data[j].media_url)
                  }
                }
              }
            }
            }
          }
        } else {
          this.instaImages = [];
        }

        // this.instaImages = res.data;
        //this.access_token1 =  res.access_token;
        // await this.http.get('https://graph.instagram.com/{media-id}', {
        //   params: {
        //     "fields": "username",
        //     "access_token": res.access_token
        //   }
        // }).subscribe((res: any) => {
        //   if (res) {
        //    localStorage.setItem("username", JSON.stringify(res.data[0].username));
        //    }
        //   else {
        //     alert("Something went wrong")
        //   }
        // })
      }
      else {
        alert("Something went wrong")
      }
    })
  }

  selectImage(image) {
    this.selectedImage = image;
    this.postForm.patchValue({
      imageFile: this.selectedImage
    });
  }

  getProducts() {

    this.web3.checkAndInstantiateWeb3()
      .then((checkConn: any) => {
        if (checkConn === 'connected') {
          this.web3.loadBlockChainData()
            .then((accountData: any) => {
              this.accountNumber = accountData[0];
              this.web3.getEtherBalance(this.accountNumber)
                .then((data: any) => {
                  this.balance = Number(data).toFixed(2);
                });
              this.web3.getContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.everlens = contractRes;
                    // this.everlens.methods.productCount()
                    //   .call()
                    //   .then(value => {
                    //     for (let i = 1; i <= value; i++) {
                    //       const product = this.everlens.methods.products(i)
                    //         .call()
                    //         .then(products => {
                    //           this.show = false;
                    //           this.totalProduct.push(products);
                    //           this.cd.detectChanges();
                    //         });
                    //     }
                    //   });
                  }
                });



              this.web3.getAuctionContract()
                .then((contractRes: any) => {
                  if (contractRes) {
                    this.auctionn = contractRes;

                  }
                });
            }, err => {
            });
        }
      }, err => {
        //    alert(err);
      });
  }

  async createNFTFromAssetData(content, options) {
    this.mintingAuction = true;

    this.submitting = true;
    let that = this;
    const ipfsAddOptions = {
      cidVersion: 1,
      hashAlg: 'sha2-256'
    }
    var d;
    const data = await this.http.get(content, { responseType: 'arraybuffer' })
      .subscribe(async data => {

        var image_data = new Uint8Array(data);
        // const filePath = options.path || 'asset.bin'
        // const basename = path.basename(filePath)
        // const ipfsPath = '/nft/' + basename;
        var assetURI, metadataURI;
        const { cid: assetCid } = await this.ipfs.add({ content: image_data }, ipfsAddOptions)

        assetURI = await this.ensureIpfsUriPrefix(assetCid.string)

        const metadata = await this.makeNFTMetadata(assetURI, options);

        const { cid: assetMCid } = await this.ipfs.add(Buffer.from(JSON.stringify(metadata)), ipfsAddOptions);

        metadataURI = await this.ensureIpfsUriPrefix(assetMCid.string);

        const tokenId = await this.mintToken(this.accountNumber, metadataURI, options, content);


        var startPrice = await this.web3.getValueToWei(options.minBidAmount);

        var endPrice = await this.web3.getValueToWei('0');

        var today = new Date();
        today.setHours(today.getHours() + 1);

        var unix_time = today.getTime() / 1000

        var repositoryAddress = await this.web3.getRepositoryAddress();


        var address = await this.everlens.methods.getAddress().call({ from: this.accountNumber });

        this.creatingAunction = true;
        var startDate = new Date().getTime() / 1000;
        var endDate = await this.web3.getValueToWei('0');

        await this.auctionn.methods.createAuction(repositoryAddress, tokenId, options.name, metadataURI, startPrice, endPrice, startDate.toFixed(0), endDate).send({ from: this.accountNumber }).then(async (val) => {
          this.creatingAunction = false;
          this.creatingAunctionCompleted = true;
          var auction_id = val.events.AuctionCreated.returnValues._auctionId

          setTimeout(() => {
            this.closeModal();
            this.router.navigate(['/home']);
          }, 2000);

        });

      })

  }

  private async ensureIpfsUriPrefix(cidOrURI) {
    let uri = cidOrURI.toString()
    uri = 'https://ipfs.io/ipfs/' + cidOrURI
    return uri
  }

  /**
* Create a new NFT token that references the given metadata CID, owned by the given address.
* 
* @param {string} ownerAddress - the ethereum address that should own the new token
* @param {string} metadataURI - IPFS URI for the NFT metadata that should be associated with this token
* @returns {Promise<string>} - the ID of the new token
*/
  private async mintToken(ownerAddress, metadataURI, options, image) {
    // the smart contract adds an ipfs:// prefix to all URIs, so make sure it doesn't get added twice
    // metadataURI = this.stripIpfsUriPrefix(metadataURI)
    let transferId;
    // Call the mintToken method to issue a new token to the given address
    // This returns a transaction object, but the transaction hasn't been confirmed
    // yet, so it doesn't have our token id.


    const tx = await this.everlens.methods.createProduct(ownerAddress, metadataURI, options.name, image, this.userName, this.userName).send({ from: this.accountNumber })
      .once('receipt', (receipt) => {
        this.mintingAuction = false;
        this.mintingAuctionCompleted = true;
        transferId = receipt.events.ProductCreated.returnValues.id
        this.submitting = false;

      }, err => {
        this.submitting = false;
      });

    return transferId
  }

  // async getNFTMetadata(tokenId) {
  //   const metadataURI = await this.everlens.methods.tokenURI(tokenId).call({ from: this.accountNumber });
  //   var uri = metadataURI.slice('https://ipfs.io/ipfs/'.length)
  //   const source = await this.ipfs.cat(uri)
  //   let contents = ''
  //   const decoder = new TextDecoder('utf-8')

  //   for await (const chunk of source) {
  //     contents += decoder.decode(chunk, {
  //       stream: true
  //     })
  //   }

  //   contents += decoder.decode()

  //   return metadataURI
  // }

  /**
   * Helper to construct metadata JSON for 
   * @param {string} assetCid - IPFS URI for the NFT asset
   * @param {object} options
   * @param {?string} name - optional name to set in NFT metadata
   * @param {?string} description - optional description to store in NFT metadata
   * @returns {object} - NFT metadata object
   */
  async makeNFTMetadata(assetURI, options) {
    const { name, description, minBidAmount } = options;
    // assetURI = this.ensureIpfsUriPrefix(assetURI)
    return {
      name,
      description,
      minBidAmount,
      image: assetURI
    }
  }

  saveit() {
    let url = "https://api.instagram.com/oauth/authorize?client_id=2864559007130049&redirect_uri=https://app.everlens.io/create-post/&scope=user_profile,user_media&response_type=code";
    window.open(url, '_self');
  }



}

