import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalModule } from 'ngx-bootstrap/modal';

import { NoMetaMaskRoutingModule } from './no-meta-mask-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NoMetaMaskRoutingModule,
    ModalModule.forRoot()
  ]
})
export class NoMetaMaskModule { }
