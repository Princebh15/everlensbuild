import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoMetaMaskComponent } from './no-meta-mask.component';

describe('NoMetaMaskComponent', () => {
  let component: NoMetaMaskComponent;
  let fixture: ComponentFixture<NoMetaMaskComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoMetaMaskComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoMetaMaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
