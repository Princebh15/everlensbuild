import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-personal-info',
  templateUrl: './personal-info.component.html',
  styleUrls: ['./personal-info.component.scss']
})
export class PersonalInfoComponent implements OnInit {
  userName: any;

  constructor() {
    this.userName = JSON.parse(localStorage.getItem('username'));

  }

  ngOnInit(): void {
    window.scrollTo({
      top: 0,
      left: 0
    })
  }

}
