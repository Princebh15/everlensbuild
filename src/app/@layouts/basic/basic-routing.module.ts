import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WorkComponent } from 'src/app/work/work.component';
import { BasicComponent } from './basic.component';

const routes: Routes = [
  {
    path: '',
    component: BasicComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('../../@pages/home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'home',
        loadChildren: () => import('../../@pages/home/home.module').then((m) => m.HomeModule),
      },
      {
        path: 'detail-page/:id',
        loadChildren: () => import('../../@pages/detail-page/detail-page.module').then((m) => m.DetailPageModule),
      },
      {
        path: 'create-post',
        loadChildren: () => import('../../@pages/create-post/create-post.module').then((m) => m.CreatePostModule),
      },
      {
        path: 'my-item',
        loadChildren: () => import('../../@pages/my-item/my-item.module').then((m) => m.MyItemModule),
      },
      {
        path: 'item-details/:id',
        loadChildren: () => import('../../@pages/item-details/item-details.module').then((m) => m.ItemDetailsModule),
      },
      {
        path: 'my-account',
        loadChildren: () => import('../../@pages/my-account/my-account.module').then((m) => m.MyAccountModule),
      },
      {
        path: 'personal-info',
        loadChildren: () => import('../../@pages/personal-info/personal-info.module').then((m) => m.PersonalInfoModule),
      }, {
        path: 'edit-info',
        loadChildren: () => import('../../@pages/edit-info/edit-info.module').then((m) => m.EditInfoModule),
      },
      {
        path: 'connect-wallet',
        loadChildren: () => import('../../@pages/connect-wallet/connect-wallet.module').then((m) => m.ConnectWalletModule),
      },
      {
        path: 'wallet',
        loadChildren: () => import('../../@pages/wallet/wallet.module').then((m) => m.WalletModule),
      },
      {
        path: 'change-password',
        loadChildren: () => import('../../@pages/change-password/change-password.module').then((m) => m.ChangePasswordModule),
      },
      {
        path: "contact-us",
        loadChildren: () => import("../../@pages/contact-us/contact-us.module").then((m) => m.ContactUsModule)
      },
      {
        path: 'works',
        component: WorkComponent,
      },
      {
        path: "**",
        loadChildren: () => import("../../@pages/not-found/not-found.module").then((m) => m.NotFoundModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicRoutingModule { }
