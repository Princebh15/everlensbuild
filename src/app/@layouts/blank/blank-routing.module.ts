import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlankComponent } from './blank.component';

const routes: Routes = [
  {
    path:'',
    component:BlankComponent,
    children:[
      {
        path:'',
        loadChildren:()=>import('../../@pages/sign-up/sign-up.module').then((m)=> m.SignUpModule),
      },
      {
        path:'sign-in',
        loadChildren:()=> import('../../@pages/sign-in/sign-in.module').then((m)=> m.SignInModule),
      },
      {
        path:'forget-password',
        loadChildren:()=> import('../../@pages/forget-password/forget-password.module').then((m) => m.ForgetPasswordModule),
      },
      {
        path:"**",
        loadChildren:()=> import("../../@pages/not-found/not-found.module").then((m)=> m.NotFoundModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BlankRoutingModule { }
