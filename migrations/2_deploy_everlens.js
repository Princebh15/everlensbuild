const Everlens = artifacts.require("Everlens");
const AuctionRepository = artifacts.require("AuctionRepository");
// DeedRepository => 0xbb55adc67f64d1e6f08ba7523ecd2eca2ee434a3
module.exports = function (deployer) {
  deployer.deploy(Everlens).then(function() {
    // then deploy NFTDutchAuction
    return deployer.deploy(AuctionRepository, Everlens.address);
  });
}